package com.example.demo;

import com.example.demo.model.PlantInventoryEntry;
import com.example.demo.model.PlantInventoryEntryRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.math.BigDecimal;

@SpringBootApplication
public class DemostracionApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(DemostracionApplication.class, args);

        PlantInventoryEntryRepository repo = ctx.getBean(PlantInventoryEntryRepository.class);
        System.out.println(repo.findSomething(BigDecimal.valueOf(200)));
    }

}

