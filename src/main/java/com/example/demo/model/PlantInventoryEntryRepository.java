package com.example.demo.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface PlantInventoryEntryRepository extends JpaRepository<PlantInventoryEntry, Long>, AdvancedMethodsForPlantInventoryEntry {

    public List<PlantInventoryEntry> findByNameLike(String name);
    @Query("Select p from PlantInventoryEntry p where p.price > ?1")
    public List<PlantInventoryEntry> findSomething(BigDecimal precio);
}
