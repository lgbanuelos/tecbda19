package com.example.demo.model;

import java.time.LocalDate;
import java.util.List;

public interface AdvancedMethodsForPlantInventoryEntry {
    List<PlantInventoryEntry> findAllPlantsAvailableForPeriod(String name, LocalDate startDate, LocalDate endDate);
}
